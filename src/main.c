#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "sort.h"


void print_arr(unsigned int *arr, unsigned int len);
bool check_sorted(unsigned int *arr, unsigned int len);


int main(void) {
	unsigned int stack[] = { 9, 6, 8, 0, 4, 5, 2, 7, 1, 3, 16, 3, 2003, 1337, 420, 69, 16, 49, 32, 7, 42 };
	unsigned int len = sizeof(stack) / sizeof(stack[0]);

	printf("Before sort:\n");
	print_arr(stack, len);

	radix_sort(stack, len);
	printf("\n");

	printf("After sort:\n");
	print_arr(stack, len);

	printf("%ssorted\n", check_sorted(stack, len) ? "" : "not ");

	return EXIT_SUCCESS;
}

void print_arr(unsigned int *arr, unsigned int len) {
	for (int i = 0; i != len; ++i)
		printf("%i ", arr[i]);
	printf("\n");
}

bool check_sorted(unsigned int *arr, unsigned int len) {
	for (int i = 1; i != len; ++i)
		if (arr[i - 1] > arr[i])
			return false;
	return true;
}
