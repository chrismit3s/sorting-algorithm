#ifndef SORT_H
#define SORT_H

#define BIT(x, n) (((x) >> (n)) & 1)

void swap(unsigned int*, unsigned int*);
int *radix_sort(unsigned int*, unsigned int);
void _radix_sort(unsigned int*, unsigned int, unsigned char);

#endif // SORT_H
