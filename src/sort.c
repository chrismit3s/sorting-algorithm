#include "sort.h"

void swap(unsigned int* restrict x, unsigned int* restrict y) {
	int a = *x;
	*x = *y;
	*y = a;
}

int* radix_sort(unsigned int* arr, unsigned int len) {
	// get all set bits in one number
	unsigned int a = 0U;
	for (unsigned int i = 0U; i != len; ++i)
		a |= arr[i];

	// check if there are set bits
	if (a != 0) {
		// get most significant bit which is not 0
		unsigned char bit = sizeof(arr[0]) * 8 - 1;
		while (BIT(a, bit) == 0)
			--bit;

		_radix_sort(arr, len, bit);
	}
	return arr;
}

void _radix_sort(unsigned int* arr, unsigned int len, unsigned char bit) {
	unsigned int index = 0U;
	for (unsigned int i = 0U; i != len; ++i)
		if (BIT(arr[i], bit) == 0)
			swap((arr + i), (arr + index++));

	if (bit != 0 && index > 1)
		_radix_sort(arr, index, bit - 1);
	if (bit != 0 && len - index > 1)
		_radix_sort((arr + index), len - index, bit - 1);
}
