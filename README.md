# Some sort of sorting algorithm

Some sort of a sorting algrithm, kinda like MSD base2 radix sort, but not really.
Actually, it's just MSD base2 radix sort.

Disclaimer: it only works with whole, non-negative integers.

## How it works

At first, it finds the most significant bit which is not 0.
Then, it reorders the array so that all numbers which have that bit set are up top.
It splits the array, decreases the bit and does the same thing again.